'use strict';

const cuid = require('cuid');
const Error = require('@scola/error');
const EventHandler = require('@scola/events');

class AbstractConnection extends EventHandler {
  constructor(filters, serial, message) {
    super();

    this.id = cuid();
    this.filters = filters;
    this.messageProvider = message;

    this.serialReceive = serial.get()
      .setProcessor(this.processReceive.bind(this));

    this.serialSend = serial.get()
      .setProcessor(this.processSend.bind(this));

    this.connection = null;
  }

  getId() {
    return this.id;
  }

  open() {
    throw new Error('not_implemented');
  }

  close(code, reason) {
    this.connection.close(code, reason);
    this.handleClose(code, reason);

    return this;
  }

  canSend() {
    return this.connection.readyState === this.connection.OPEN;
  }

  send(message) {
    this.emit('debug', this, 'send', message);

    return this.serialSend
      .process(this.filters.slice().reverse(), [message])
      .then(this.handleSend.bind(this, message))
      .catch(this.handleSendError.bind(this, message));
  }

  bindListeners() {
    this.bindListener('close', this.connection, this.handleClose);
    this.bindListener('error', this.connection, this.handleError);
    this.bindListener('message', this.connection, this.handleMessage);
  }

  unbindListeners() {
    this.unbindListener('close', this.connection, this.handleClose);
    this.unbindListener('error', this.connection, this.handleError);
    this.unbindListener('message', this.connection, this.handleMessage);
  }

  handleClose(code, reason) {
    this.emit('close', this, code, reason);
  }

  handleError(error) {
    this.emit('error', new Error('transport_connection_error', {
      origin: error
    }));
  }

  handleMessage(data) {
    this.emit('debug', this, 'handleMessage', data);

    const message = this.messageProvider
      .get()
      .setConnection(this)
      .setBody(data);

    return this.serialReceive
      .process(this.filters, [message])
      .then(this.handleReceive.bind(this, message))
      .catch(this.handleReceiveError.bind(this, message));
  }

  processReceive(filter, [message]) {
    this.emit('debug', this, 'processReceive', message);
    return filter.get().receive(message);
  }

  handleReceive(message) {
    this.emit('message', message);
  }

  handleReceiveError(message, error) {
    this.emit('error', new Error('transport_message_not_received', {
      origin: error,
      detail: {
        message
      }
    }));
  }

  processSend(filter, [message]) {
    this.emit('debug', this, 'processSend', message);
    return filter.get().send(message);
  }

  handleSend(message) {
    this.emit('debug', this, 'handleSend', message);

    this.connection.send(
      message.getBody()
    );
  }

  handleSendError(message, error) {
    this.emit('error', new Error('transport_message_not_sent', {
      origin: error,
      detail: {
        message
      }
    }));
  }
}

module.exports = AbstractConnection;
